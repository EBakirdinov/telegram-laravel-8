<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/set', [HomeController::class, 'setWebHook']);
Route::post('/5544704228:AAGlZv1kmvzOoSxUyZvdSvEPjhCQ0flsnfo/webhook', [HomeController::class, 'getWebhookUpdates']);
Route::match(['get', 'post'], '/getupdates', [HomeController::class, 'getUpdates'])->name('get_updates');