<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Bavix\Wallet\Traits\CanPay;
use Bavix\Wallet\Interfaces\Customer;

/**
 * @mixin \Eloquent
 * @mixin IdeHelperUser
 */
class User extends Model implements Customer
{
    use CanPay;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'avatar', 'provider_id', 'email_verified_at', 'verification_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function wishlists()
    {
        return $this->hasMany(Wishlist::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function wallets()
    {
        return $this->hasMany(Wallet::class)->orderBy('created_at', 'desc');
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function shop()
    {
        return $this->hasOne(Shop::class);
    }

    public function followed_shops()
    {
        return $this->belongsToMany(Shop::class, 'shop_followers', 'user_id', 'shop_id');
    }

    public function getPhoneReformat(){
        return preg_replace("/[^0-9]/", "", $this->phone);
    }
}
