<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;

/**
 * @mixin \Eloquent
 * @mixin IdeHelperAttribute
 */
class Attribute extends Model
{
    public function getTranslation($field = '', $lang = false){
		$lang = $lang == false ? App::getLocale() : $lang;
		$attribute_translation = $this->hasMany(AttributeTranslation::class)->where('lang', $lang)->first();
		return $attribute_translation != null ? $attribute_translation->$field : $this->$field;
    }

    public function attribute_translations(){
    	return $this->hasMany(AttributeTranslation::class);
    }

    public function attribute_values(){
        return $this->hasmany(AttributeValue::class);
    }
    public function product_values($product_id){
        return $this->hasmany(AttributeValue::class)->where('product_id',$product_id);
    }

}
