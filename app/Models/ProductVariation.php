<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 * @mixin IdeHelperProductVariation
 */
class ProductVariation extends Model
{
    protected $fillable = [
        'product_id',
        'code',
        'sku',
        'price',
        'stock',
        'img',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function combinations()
    {
        return $this->hasMany(ProductVariationCombination::class);
    }
}
