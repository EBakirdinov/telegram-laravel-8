<?php

namespace App\Http\Controllers;

use App\Http\Commands\Traits\CustomTraits;
use Illuminate\Http\Request;

use Telegram;
use App\Models\Wallet;
use App\Models\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Attribute;
use App\Models\ProductAttributeValue;

use PaytmWallet;
use Telegram\Bot\Laravel\Facades\Telegram as FacadesTelegram;

class HomeController extends Controller
{
    use \App\Http\Commands\Traits\Globals;
    use \App\Http\Commands\Traits\CustomMethods;

    public function index() {
        return view('welcome');
    }

    public function setWebHook() {
        $response = Telegram::setWebhook([
            'url' => 'https://flabio.ru/5544704228:AAGlZv1kmvzOoSxUyZvdSvEPjhCQ0flsnfo/webhook']
        );
    }

    public function getWebhookUpdates(Request $request) {
        $update = Telegram::getWebhookUpdate();
        
        $updateArray = $update->toArray();
        $command = null;
        if (isset($updateArray['callback_query'])) {
            foreach (explode(":", $updateArray['callback_query']['data']) as $value) {
                $value = explode("-", $value);
                $data[$value[0]] = $value[1];
            }
            $command = $data['command'];
        } else {
            $command = (substr($updateArray['message']['text'], 0, 1) === '/') ? str_replace('/', '', $updateArray['message']['text']) : array_search($updateArray['message']['text'], $this->commandTexts);
        }
        if ($command) Telegram::triggerCommand($command, $update);

        return 1;
    }

    public function getUpdates(Request $request) {
        if ($request->isXmlHttpRequest()) {
            $updates = Telegram::commandsHandler(false);

            foreach ($updates as $update) {
                $updateArray = $update->toArray();
                $command = null;
                if (isset($updateArray['callback_query'])) {
                    foreach (explode(":", $updateArray['callback_query']['data']) as $value) {
                        $value = explode("-", $value);
                        $data[$value[0]] = $value[1];
                    }
                    $command = $data['command'];
                } else {
                    $command = (substr($updateArray['message']['text'], 0, 1) === '/') ? str_replace('/', '', $updateArray['message']['text']) : array_search($updateArray['message']['text'], $this->commandTexts);
                }
                if ($command) Telegram::triggerCommand($command, $update);
            }

            return response()->json($updates);
        }
    }
}
