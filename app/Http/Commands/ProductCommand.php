<?php

namespace App\Http\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductAttributeValue;
use App\Models\User;

use Telegram;
use Telegram\Bot\FileUpload\InputFile;

class ProductCommand extends Command
{
    use Traits\CustomMethods;
    use Traits\Globals;

    /**
     * @var string Command Name
     */
    protected $name = "product";

    /**
     * @var string Command Description
     */
    protected $description = "Start Command to get you started";

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $data = $this->getUpdate();
        if ($data->callback_query) $callbackData = $this->parseCallbackData($data);
        
        $this->maxRowItems = 5;
        $id = (isset($callbackData) && isset($callbackData['id'])) ? intval($callbackData['id']) : null;
        $productId = (isset($callbackData) && isset($callbackData['product_id'])) ? intval($callbackData['product_id']) : null;
        $fromProduct = (isset($callbackData) && isset($callbackData['from_product'])) ? boolval($callbackData['from_product']) : 0;
        $page = (isset($callbackData) && isset($callbackData['page'])) ? intval($callbackData['page']) : 1;
        $action = (isset($callbackData) && isset($callbackData['action'])) ? $callbackData['action'] : null;
        $markup[$this->markupType][] = array();

        $user = User::where('telegram_id', ($data->callback_query) ? $data->callback_query->from->id : $data->message->from->id)->get()->first();

        if ($productId != null) {
            // getting data from DB
            $query = Product::find($productId);
            $attributes = ProductAttributeValue::where('product_id', $productId)->with(['attribute', 'value'])->get();

            if ($action != null) {
                switch ($action) {
                    case 'buy':
                        $user->safePay($query);
                    default:
                        break;
                }
            }

            $user->refresh();
            $text = $this->createProductText($query, $attributes);

            $text .= "<i>\r\n\r\n\r\n\r\nВаш баланс: ".$user->wallet->balance."</i>";

            if ($query->getAmountProduct($user) <= $user->wallet->balance) $markup[$this->markupType][] = [['text' => "Купить", 'callback_data' => 'command-product:product_id-'. $productId . ':action-buy']];
            $markup[$this->markupType][] = [['text' => "Страница товара", 'url' => "https://azor.kg"]];
            $markup[$this->markupType][] = $this->createBackButton('command-product:id-'. $id .(($productId) ? ':from_product-true' : ''). ':page-'.$page);
        } else {
            // getting data from DB
            $query = Product::whereHas('product_categories', function($query) use ($id) { 
                $query->where('category_id', $id);
            })->paginate($this->paginationCount[$this->name], ['*'], 'page', $page);

            // IF NO DATA
            if ($query->getCollection('data')->isEmpty()) {
                // if empty
                $text = $this->emptyText[$this->name];
            } else {
                // creating content
                $text = $this->createHeadText($this->titleText[$this->name]." ".Category::find($id)->name, $query) . "\r\n\r\n";
                $index = 0;
                foreach (array_values($query->getCollection('data')->toArray()) as $key => $item) {
                    $text = $text . ($key + 1) . ". " . $item['name'] . "\r\n";
                    $markup[$this->markupType][((isset($markup[$this->markupType][$index]) && count($markup[$this->markupType][$index]) >= ($this->maxRowItems-1))) ? $index++ : $index][] = ['text' => $key+1, 'callback_data' => 'command-product:id-'.$id.':product_id-'.$item['id'].':page-'.$page];
                };
                $markup[$this->markupType][] = $this->createPaginationButtons($this->name, $query, $id);
                $markup[$this->markupType][] = $this->createBackButton('command-category:id-'. Category::find($id)->parent_id);
            };
        };
        // print_r($markup);exit;
        if ($productId != null || $fromProduct) {
            Telegram::deleteMessage([
                'chat_id' => $data->callback_query->message->chat->id,
                'message_id' => $data->callback_query->message->message_id,
            ]);
        };

        // response
        if (isset($callbackData) && !$fromProduct) {
            if ($productId != null) {
                $this->replyWithPhoto([
                    'text' => $text,
                    'caption' => $text,
                    'photo'=> InputFile::create('https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg', 'test'),
                    'parse_mode' => 'html',
                    'reply_markup' => json_encode($markup),
                ]);
            } else {
                Telegram::editMessageText([
                    'chat_id' => $data->callback_query->message->chat->id,
                    'message_id' => $data->callback_query->message->message_id,
                    'text' => $text,
                    'parse_mode' => 'html',
                    'reply_markup' => json_encode($markup),
                ]);
            }
        } else {
            $this->replyWithMessage([
                'text' => $text,
                'parse_mode' => 'html',
                'reply_markup' => (isset($markup) && !empty($markup)) ? json_encode($markup) : false,
            ]);
        }
    }
}