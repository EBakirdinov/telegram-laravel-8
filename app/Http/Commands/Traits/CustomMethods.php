<?php

namespace App\Http\Commands\Traits;

trait CustomMethods
{
    public function parseCallbackData($data) {   
        $arguments = array();
        foreach (explode(":", $data->callback_query->data) as $value) {
            $value = explode("-", $value);
            $arguments[$value[0]] = $value[1];
        }
        return $arguments;
    }

    public function createHeadText($text, $data) {  
        return "<b>" . $text . "</b> <i>(" . $data->currentPage() . "-" . $data->lastPage() . ")</i>";
    }

    public function createPaginationButtons($command, $data, $parentId = null) {   
        $array = array();
        if ($data->currentPage() > 1) {
            array_push(
                $array, 
                // ['text' => '« 1', 'callback_data' => 'command-category:page-1'],
                ['text' => '‹ ' . ($data->currentPage() - 1), 'callback_data' => 'command-'. $command .':page-'.($data->currentPage() - 1).(($parentId != null) ? ':id-'.$parentId : '')],
            );
        }
        array_push(
            $array, 
            ['text' => "• " . $data->currentPage() . " •", 'callback_data' => 'azor']
        );
        if ($data->currentPage() < $data->lastPage()) {
            array_push(
                $array, 
                ['text' => ($data->currentPage()+1) . ' ›', 'callback_data' => 'command-'. $command .':page-'.($data->currentPage()+1).(($parentId != null) ? ':id-'.$parentId : '')],
                // ['text' => $data->lastPage() . ' »', 'callback_data' => 'command-category:page-' . $data->lastPage()]
            );
        }
        return $array;
    }

    public function createBackButton($data) {   
        $array = array();
        array_push(
            $array, 
            ['text' => "Назад", 'callback_data' => $data]
        );

        return $array;
    }

    public function getParentCategories($data) {
        $array = array();
        do {
            $array[] = $data['name'];
            if (!$data['parent_categories']) {
                break;
            }
            $data = $data['parent_categories'];
        } while (true);
        return $array;
    }

    public function createUserText($data, $type = 'user_info') {
        if (!$data) {
            return null;
        };

        $replyData = array();
        switch ($type) {
            case 'user_info':        
                $replyData[] = '<b>Имя</b>: '.$data->name; // name
                $replyData[] = '<b>Баланс</b>: '.$data->wallet->balance .' '. $this->currency; // name
                $replyData[] = '<b>Почта</b>: '.$data->email; // email
                $replyData[] = '<b>Номер телефона</b>: '.$data->phone; // phones
                break;
            case 'user_order':    
                $index = 1;
                foreach ($data->toArray() as $item) {
                    $dataArray = [];
                    if ($item['meta']['description'] == 'purchase') {
                        $dataArray[] = $item['meta']['title'];
                        $dataArray[] = $item['meta']['amount'] .' '. $this->currency;
                        $dataArray[] = date('Y-m-d H:i', strtotime($item['created_at']));
                        $replyData[] = $index . ". ". implode(" • ", $dataArray);
                        $index++;
                    }
                }
            default:
                break;
        }

        return implode("\r\n\r\n", $replyData);
    }

    public function createProductText($data, $attributes) {
        if (!$data) {
            return null;
        };
        
        $replyData = array();
        if (!$attributes->isEmpty()) {
            $attributesData = array();
            foreach ($attributes as $attribute) {
                $attributesData[$attribute->attribute->name][] = $attribute->value->name;
            };
            $attributesText = implode("\r\n", array_map(
                function ($v, $k) {
                    return $k.': '.implode(", ", $v);
                }, 
                $attributesData, 
                array_keys($attributesData)
            ));
        };

        if ($data->name) $replyData[] = '<b>'.$data->name.'</b>'; // name
        if ($data->lowest_price) {                 //
            $replyData[] = $data->lowest_price .' '. $this->currency;    //
        } elseif ($data->highest_price) {          // price
            $replyData[] = $data->highest_price .' '. $this->currency;  //
        };
        if ($data->description) $replyData[] = strip_tags($data->description, $this->supportedTags); // description
        if (!$attributes->isEmpty()) $replyData[] = "<b>Спецификация</b>:\r\n".$attributesText; // attributes

        return implode("\r\n\r\n", $replyData);
    }
}