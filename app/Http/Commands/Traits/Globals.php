<?php

namespace App\Http\Commands\Traits;

trait Globals
{
    /**
     * @var array
     */
    protected $paginationCount = array(
        'shop' => 6,
        'category' => 8,
        'product' => 10
    );

    /**
     * @var array
     */
    protected $titleText = array(
        'start' => "Привет! Я тестовый бот Azor 🤖",
        'shop' => "Магазины",
        'category' => "Категории",
        'product' => "Товары в категории"
    );
    
    /**
     * @var array
     */
    protected $emptyText = array(
        'shop' => "Список магазинов пуст 😔",
        'category' => "Список категорий пуст 😔",
        'product' => "Список товаров пуст 😔"
    );

    /**
     * @var array
     */
    protected $commandTexts = array(
        'category' => 'Категории 📔',
        'shop' => 'Магазины 🛍️',
        'cabinet' => 'Личный кабинет 👤',
    );

    /**
     * @var integer
     */
    protected $maxRowItems;

    /**
     * @var string
     */
    protected $markupType = 'inline_keyboard';

    /**
     * @var string
     */
    protected $currency = 'сом';

    /**
     * @var array
     */
    protected $supportedTags = array('<b>', '<strong>', '<i>', '<em>', '<u>', '<ins>', '<s>', '<strike>', '<del>', '<span>', '<tg-spoiler>', '<b>', '<code>', '<pre>');
}