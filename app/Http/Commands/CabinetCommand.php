<?php

namespace App\Http\Commands;

use Telegram\Bot\Commands\Command;
use Telegram\Bot\FileUpload\InputFile;

use App\Models\User;
use App\Models\Order;

use Telegram;

class CabinetCommand extends Command
{
    use Traits\Globals;
    use Traits\CustomMethods;

    /**
     * @var string Command Name
     */
    protected $name = "cabinet";

    /**
     * @var string Command Description
     */
    protected $description = "Start Command to get you started";

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $data = $this->getUpdate();
        if ($data->callback_query) $callbackData = $this->parseCallbackData($data);

        $getOrderHistory = (isset($callbackData) && isset($callbackData['get_orders'])) ? boolval($callbackData['get_orders']) : false;
        $user = User::where('telegram_id', ($data->callback_query) ? $data->callback_query->from->id : $data->message->from->id)->get()->first();
        $fromSubPage = (isset($callbackData) && isset($callbackData['from_sub'])) ? boolval($callbackData['from_sub']) : 0;
        $markup[$this->markupType][] = array();
        $text = "";

        if ($getOrderHistory) {
            $text = $this->createUserText($user->walletTransactions, 'user_order');
            $markup[$this->markupType][] = [['text' => "Назад", 'callback_data' => "command-cabinet:from_sub-true"]];
        } else {
            if ($user) {
                $text = $this->createUserText($user, 'user_info');
                $markup[$this->markupType][] = [['text' => "История покупок", 'callback_data' => "command-cabinet:get_orders-true"]];
            } else {
            }
        };

        if ($getOrderHistory || $fromSubPage) {
            Telegram::deleteMessage([
                'chat_id' => $data->callback_query->message->chat->id,
                'message_id' => $data->callback_query->message->message_id,
            ]);
        };

        if ($getOrderHistory) {
            $this->replyWithMessage([
                'text' => $text,
                'parse_mode' => 'html',
                'reply_markup' => (isset($markup) && !empty($markup)) ? json_encode($markup) : false,
            ]);
        } else {
            $this->replyWithPhoto([
                'text' => $text,
                'caption' => $text,
                'photo'=> InputFile::create('https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg', 'test'),
                'parse_mode' => 'html',
                'reply_markup' => json_encode($markup),
            ]);
        }
    }   
}